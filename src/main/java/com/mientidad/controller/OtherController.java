package com.mientidad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mientidad.dto.ResponseDTO;
import com.mientidad.entities.Activofijo;
import com.mientidad.entities.Areaempresa;
import com.mientidad.entities.Persona;
import com.mientidad.service.IOtherService;

@RestController
@RequestMapping("/api")
public class OtherController {

	
	@Autowired
	private IOtherService otherService;
	
	@GetMapping("/persona")
	public ResponseEntity<ResponseDTO> listarPersonas(){
		System.out.println("prueba");
		ResponseDTO respuesta = new ResponseDTO();
		try {
			List<Persona> listaPersonas = otherService.listarPersonas();
			if (listaPersonas.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaPersonas);
			}
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
	
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);

	}
	
	@GetMapping("/area")
	public ResponseEntity<ResponseDTO> listarAreas(){
		System.out.println("prueba");
		ResponseDTO respuesta = new ResponseDTO();
		try {
			List<Areaempresa> listaAreas = otherService.listarAreas();
			if (listaAreas.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaAreas);
			}
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
	
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);

	}
}
