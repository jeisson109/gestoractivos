package com.mientidad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mientidad.dto.ActualizarDTO;
import com.mientidad.dto.ResponseDTO;
import com.mientidad.entities.Activofijo;
import com.mientidad.service.IActivoFijoService;

@RestController
@RequestMapping("/api/activofijo")
public class ActivoFijoController {

	@Autowired
	private IActivoFijoService activoFijoService;
	
	@GetMapping
	public ResponseEntity<ResponseDTO> listarActivos(){
		System.out.println("prueba");
		ResponseDTO respuesta = new ResponseDTO();
		try {
			List<Activofijo> listaActivos = activoFijoService.listarActivos();
			if (listaActivos.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaActivos);
			}
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
	
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);
	}

	@GetMapping("/fecha/{fecha}")
	public ResponseEntity<ResponseDTO>  listarActivosByFecha(@PathVariable String fecha){
		ResponseDTO respuesta = new ResponseDTO();
		try {
			List<Activofijo> listaActivos = activoFijoService.listarActivosByFecha(fecha);
			if(listaActivos == null){
				respuesta.setCode(400);
				respuesta.setDescription("El campo fecha no esta en el formato yyyy-MM-dd");
			}else if (listaActivos.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaActivos);
			}
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
		
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build().ok().body(respuesta) :  ResponseEntity.internalServerError().body(respuesta);
	}
	
	@GetMapping("/tipo/{tipo}")
	public ResponseEntity<ResponseDTO> listarActivosByTipo(@PathVariable Integer tipo){
		System.out.println("prueba");
		ResponseDTO respuesta = new ResponseDTO();
		try {
			List<Activofijo> listaActivos = activoFijoService.listarActivosByTipo(tipo);
			if (listaActivos.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaActivos);
			}
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}

		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);
	}
	
	@GetMapping("/serial/{serial}")
	public ResponseEntity<ResponseDTO> listarActivosBySerial(@PathVariable Integer serial ){
		System.out.println("prueba");
		ResponseDTO respuesta = new ResponseDTO();
		try {
			List<Activofijo> listaActivos = activoFijoService.listarActivosBySerial(serial);
			if (listaActivos.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaActivos);
			}
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
		
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);
	}
	
	@PostMapping
	public ResponseEntity<ResponseDTO> crearActivo(@RequestBody Activofijo activoFijo){
		ResponseDTO respuesta = new ResponseDTO();
		try {
			respuesta = activoFijoService.crearActivo(activoFijo);
			/*if (listaActivos.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaActivos);
			}*/
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
		
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);
	}
	
	@PatchMapping
	public ResponseEntity<ResponseDTO> actualizarActivo(@RequestBody ActualizarDTO actualizar) {
		ResponseDTO respuesta = new ResponseDTO();
		try {
			respuesta = activoFijoService.actualizarActivo(actualizar);
			/*if (listaActivos.isEmpty()) {
				respuesta.setCode(404);
				respuesta.setDescription("No hay registros");
			} else {
				respuesta.setCode(200);
				respuesta.setDescription("Se consulta exitosamente la informacion");
				respuesta.setResponse(listaActivos);
			}*/
			
		} catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
		
		return respuesta.getCode().equals(200) ? ResponseEntity.ok().body(respuesta) : respuesta.getCode().equals(404) ? ResponseEntity.notFound().build() :  ResponseEntity.internalServerError().body(respuesta);
	}
	
}
