package com.mientidad.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JEISSON
 */
@Entity
@Table(name = "activosfijos")
public class Activofijo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private Integer descripcion;
    @Column(name = "serial")
    private Integer serial;
    @Column(name = "numerointinv")
    private Integer numerointinv;
    @Column(name = "valorcompra")
    private Integer valorcompra;
    @Column(name = "fechacompra")
    @Temporal(TemporalType.DATE)
    private Date fechacompra;
    @Column(name = "fechabaja")
    @Temporal(TemporalType.DATE)
    private Date fechabaja;
    @Column(name = "color")
    private String color;
    @Column(name = "asignadoa")
    private Integer asignadoa;
    @JoinColumn(name = "areaasignada", referencedColumnName = "id")
    @ManyToOne
    private Areaempresa areaasignada;
    @JoinColumn(name = "dimension", referencedColumnName = "id")
    @ManyToOne
    private Dimension dimension;
    @JoinColumn(name = "estado", referencedColumnName = "id")
    @ManyToOne
    private Estado estado;
    @JoinColumn(name = "personaasignada", referencedColumnName = "id")
    @ManyToOne
    private Persona personaasignada;
    @JoinColumn(name = "tipoactivo", referencedColumnName = "id")
    @ManyToOne
    private Tipoactivo tipoactivo;

    public Activofijo() {
    }

    public Activofijo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(Integer descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getSerial() {
        return serial;
    }

    public void setSerial(Integer serial) {
        this.serial = serial;
    }

    public Integer getNumerointinv() {
        return numerointinv;
    }

    public void setNumerointinv(Integer numerointinv) {
        this.numerointinv = numerointinv;
    }

    public Integer getValorcompra() {
        return valorcompra;
    }

    public void setValorcompra(Integer valorcompra) {
        this.valorcompra = valorcompra;
    }

    public Date getFechacompra() {
        return fechacompra;
    }

    public void setFechacompra(Date fechacompra) {
        this.fechacompra = fechacompra;
    }

    public Date getFechabaja() {
        return fechabaja;
    }

    public void setFechabaja(Date fechabaja) {
        this.fechabaja = fechabaja;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getAsignadoa() {
        return asignadoa;
    }

    public void setAsignadoa(Integer asignadoa) {
        this.asignadoa = asignadoa;
    }

    public Areaempresa getAreaasignada() {
        return areaasignada;
    }

    public void setAreaasignada(Areaempresa areaasignada) {
        this.areaasignada = areaasignada;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Persona getPersonaasignada() {
        return personaasignada;
    }

    public void setPersonaasignada(Persona personaasignada) {
        this.personaasignada = personaasignada;
    }

    public Tipoactivo getTipoactivo() {
        return tipoactivo;
    }

    public void setTipoactivo(Tipoactivo tipoactivo) {
        this.tipoactivo = tipoactivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activofijo)) {
            return false;
        }
        Activofijo other = (Activofijo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.entities.Activosfijos[ id=" + id + " ]";
    }
    
}
