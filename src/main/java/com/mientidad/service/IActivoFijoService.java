package com.mientidad.service;

import java.util.List;

import com.mientidad.dto.ActualizarDTO;
import com.mientidad.dto.ResponseDTO;
import com.mientidad.entities.Activofijo;

public interface IActivoFijoService {
	List<Activofijo> listarActivos();
	List<Activofijo> listarActivosByFecha(String fecha);
	List<Activofijo> listarActivosByTipo(Integer tipo);
	List<Activofijo> listarActivosBySerial(Integer serial);
	ResponseDTO crearActivo(Activofijo activoFijo);
	ResponseDTO actualizarActivo(ActualizarDTO actualizar);
}
