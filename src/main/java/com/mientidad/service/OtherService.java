package com.mientidad.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mientidad.entities.Areaempresa;
import com.mientidad.entities.Persona;
import com.mientidad.repository.AreaRepository;
import com.mientidad.repository.PersonaRepository;

@Service
public class OtherService implements IOtherService {

	@Autowired
	private PersonaRepository personaRepository;
	
	@Autowired
	private AreaRepository areaRepository;

	@Override
	public List<Persona> listarPersonas() {
		List<Persona> listPersonas = personaRepository.findAll() ;
		return listPersonas;
	}

	@Override
	public List<Areaempresa> listarAreas() {
		List<Areaempresa> listAreas = areaRepository.findAll() ;
		return listAreas;
	}

}
