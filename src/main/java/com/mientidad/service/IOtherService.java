package com.mientidad.service;

import java.util.List;

import com.mientidad.entities.Areaempresa;
import com.mientidad.entities.Persona;

public interface IOtherService {
	List<Persona> listarPersonas();
	List<Areaempresa> listarAreas();
}
