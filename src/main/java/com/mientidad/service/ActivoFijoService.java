package com.mientidad.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mientidad.dto.ActualizarDTO;
import com.mientidad.dto.ResponseDTO;
import com.mientidad.entities.Activofijo;
import com.mientidad.repository.ActivoFijoRepository;

@Service
public class ActivoFijoService implements IActivoFijoService {

	@Autowired
	private ActivoFijoRepository activoFijoRepository;
	
	@Override
	public List<Activofijo> listarActivos() {
		List<Activofijo> listActivos = activoFijoRepository.findAll() ;
		return listActivos;
	}

	@Override
	public List<Activofijo> listarActivosByFecha(String fecha) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		List<Activofijo> listActivos;
		try {
			Date fechabusqueda = formato.parse(fecha);
			listActivos = activoFijoRepository.findByFechacompra(fechabusqueda) ;
			
		} catch (ParseException e) {
			return null;
		}
		return listActivos;
	}

	@Override
	public List<Activofijo> listarActivosByTipo(Integer tipo) {
		List<Activofijo> listActivos = activoFijoRepository.findByTipoactivoId(tipo) ;
		return listActivos;
	}

	@Override
	public List<Activofijo> listarActivosBySerial(Integer serial) {
		List<Activofijo> listActivos = activoFijoRepository.findBySerial(serial) ;
		return listActivos;
	}

	@Override
	public ResponseDTO crearActivo(Activofijo activoFijo) {
		ResponseDTO respuesta = new ResponseDTO();
		try {
			respuesta.setResponse(activoFijoRepository.saveAndFlush(activoFijo));	
			respuesta.setCode(200);
			respuesta.setDescription("Se crea exitosamente el objeto");
		}catch (Exception e) {
			respuesta.setCode(500);
			respuesta.setDescription("Error interno en el servidor");
		}
		return respuesta;
	}

	@Override
	public ResponseDTO actualizarActivo(ActualizarDTO actualizar) {
		ResponseDTO respuesta = new ResponseDTO();
		if (actualizar.getOp().equals("replace")) {
			Activofijo activo = activoFijoRepository.getOne(actualizar.getId());
			if (actualizar.getPath().equals("serial")){
				activo.setSerial((Integer)actualizar.getValue());
				activoFijoRepository.save(activo);
				respuesta.setCode(200);
				respuesta.setDescription("Se actualiza exitosamente la informacion");
			}
			if (actualizar.getPath().equals("fechabaja")){
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date fechacambio = formato.parse((String)actualizar.getValue());
					if (activo.getFechacompra().before(fechacambio)){
						activo.setFechabaja(fechacambio);
						activoFijoRepository.save(activo);
						respuesta.setCode(200);
						respuesta.setDescription("Se actualiza exitosamente la informacion");
						
					}else {
						respuesta.setCode(400);
						respuesta.setDescription("La fecha de baja es menor a la fecha de compra del activo");
					}

				} catch (ParseException e) {
					respuesta.setCode(400);
					respuesta.setDescription("El campo fecha no esta en el formato yyyy-MM-dd");
				}
			} 
		}
		return respuesta;
	}

}
