/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mientidad.repository;

import org.springframework.stereotype.Repository;

import com.mientidad.entities.Activofijo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jeison
 */
@Repository
public interface ActivoFijoRepository extends JpaRepository<Activofijo, Long>{
    
	List<Activofijo> findByFechacompra(Date fecha);
	List<Activofijo> findByTipoactivoId(Integer idTipo);
	List<Activofijo> findBySerial(Integer serial);
	
}
