/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mientidad.repository;

import org.springframework.stereotype.Repository;

import com.mientidad.entities.Activofijo;
import com.mientidad.entities.Areaempresa;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jeison
 */
@Repository
public interface AreaRepository extends JpaRepository<Areaempresa, Long>{
    
}
